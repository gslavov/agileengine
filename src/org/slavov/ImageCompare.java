package org.slavov;

import org.slavov.exception.PatternMismatchException;

import java.awt.Point;
import java.awt.Color;
import java.awt.Rectangle;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

public class ImageCompare {

    public static final int PIXEL_DISTANCE_CORRELATION = 10;
    public static final int COLOR_MATCH_PERCENTAGE_CORRELATION = 10;
    public static final String EXCLUDE_AREA_PATTERN = "\\[x=(\\d+),(\\s?)y=(\\d+),(\\s?)width=(\\d+),(\\s?)height=(\\d+)\\]";

    public void compare(String image1Path, String image2Path) {
        compare(image1Path, image2Path, null);
    }

    public void compare(String image1Path, String image2Path, String excludesPath) {
        try {
            File image1 = new File(image1Path);
            File image2 = new File(image2Path);
            BufferedImage bufferedImage1 = ImageIO.read(image1);
            BufferedImage bufferedImage2 = ImageIO.read(image2);
            if (bufferedImage1 != null && bufferedImage2 != null
                    && bufferedImage1.getWidth() == bufferedImage2.getWidth()
                    && bufferedImage1.getHeight() == bufferedImage2.getHeight()) {
                List<Rectangle> excludes = parseExcludes(excludesPath);
                ImageIO.write(compareImages(bufferedImage1, bufferedImage2, excludes), "png", new File("output.png"));
            } else {
                System.out.println("Both files should be images and with the same height and width");
            }
        } catch (IOException e) {
            System.err.println("Exception during images comparison ");
            e.getStackTrace();
        } catch (PatternMismatchException e) {
            System.err.println("Pattern Mismatch Exception: ");
            e.printStackTrace();
        }
    }

    public BufferedImage compareImages(BufferedImage img1, BufferedImage img2, List<Rectangle> excludes) {
        final int img1Width = img1.getWidth();
        final int img1Height = img1.getHeight();
        final int[] img1RGB = img1.getRGB(0, 0, img1Width, img1Height, null, 0, img1Width);
        List<Point> diffPoints = new ArrayList<>();
        for (int x = 0; x < img1Width; x++) {
            for (int y = 0; y < img1Height; y++) {
                Point point = new Point(x, y);
                if (!isExcluded(excludes, point)) {
                    if (isNotMatched(img1, img2, x, y)) {
                        diffPoints.add(new Point(x, y));
                    }
                }
            }
        }
        List<Set<Point>> result = splitPixelsToAreas(diffPoints);
        final BufferedImage out = new BufferedImage(img1Width, img1Height, BufferedImage.TYPE_INT_RGB);
        out.setRGB(0, 0, img1Width, img1Height, img1RGB, 0, img1Width);
        drawRectangles(out, result);
        drawExcludedRectangle(excludes, out);
        return out;
    }

    private void drawExcludedRectangle(List<Rectangle> excludes, BufferedImage out) {
        for (Rectangle exclude : excludes) {
            drawRectangle(out, exclude, Color.BLUE);
        }
    }

    private List<Rectangle> parseExcludes(String excludesPath) throws PatternMismatchException {
        List<Rectangle> excludes = new ArrayList<>();
        if (excludesPath != null && !excludesPath.isEmpty()) {
            File exclude = new File(excludesPath);
            Pattern pattern = Pattern.compile(EXCLUDE_AREA_PATTERN);
            try (BufferedReader reader = new BufferedReader(new FileReader(exclude))) {
                for (String line; (line = reader.readLine()) != null; ) {
                    if (pattern.matcher(line.trim()).matches()) {
                        Matcher matcher = Pattern.compile("\\d+").matcher(line.trim());
                        int x = 0, y = 0, width = 0, height = 0;
                        for (int i = 0; matcher.find(); i++) {
                            switch (i) {
                                case 0:
                                    x = Integer.valueOf(matcher.group());
                                    break;
                                case 1:
                                    y = Integer.valueOf(matcher.group());
                                    break;
                                case 2:
                                    width = Integer.valueOf(matcher.group());
                                    break;
                                case 3:
                                    height = Integer.valueOf(matcher.group());
                                    break;
                            }
                        }
                        excludes.add(new Rectangle(x, y, width, height));
                    } else {
                        throw new PatternMismatchException(line.trim() + " does not match pattern : [x=<x>,y=<y>,width=<w>,height=<h>]");
                    }
                }
            } catch (IOException e) {
                System.err.println("Could not read from file " + excludesPath);
                e.printStackTrace();
            }
        }
        return excludes;
    }

    private boolean isExcluded(List<Rectangle> excludes, Point point) {
        if (excludes == null || excludes.isEmpty()) {
            return false;
        }
        for (Rectangle exclude : excludes) {
            if (exclude.contains(point)) {
                return true;
            }
        }
        return false;
    }

    private boolean isNotMatched(BufferedImage img1, BufferedImage img2, int x, int y) {
        if (img1.getRGB(x, y) != img2.getRGB(x, y)) {
            Color img1Color = new Color(img1.getRGB(x, y));
            Color img2Color = new Color(img2.getRGB(x, y));
            double proc = percentageDiff(img1Color, img2Color);
            if (proc > COLOR_MATCH_PERCENTAGE_CORRELATION) {
                return true;
            }
        }
        return false;
    }

    private double percentageDiff(Color img1Color, Color img2Color) {
        //Distance between colors
        double distance = Math.sqrt(Math.pow(img2Color.getAlpha() - img1Color.getAlpha(), 2)
                + Math.pow(img2Color.getRed() - img1Color.getRed(), 2)
                + Math.pow(img2Color.getGreen() - img1Color.getGreen(), 2)
                + Math.pow(img2Color.getBlue() - img1Color.getBlue(), 2));
        //Percentage
        return (distance / Math.sqrt(255 * 255 + 255 * 255 + 255 * 255 + 255 * 255)) * 100;
    }

    private void drawRectangles(BufferedImage out, List<Set<Point>> areas) {
        for (Set<Point> area : areas) {
            drawRectangle(out, getRectangle(area), Color.RED);
        }
    }

    private void drawRectangle(BufferedImage out, Rectangle rectangle, Color color) {
        Graphics2D graph = out.createGraphics();
        graph.setColor(color);
        graph.drawRect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
        graph.dispose();
    }

    private Rectangle getRectangle(Set<Point> area) {
        int minX = Integer.MAX_VALUE;
        int minY = Integer.MAX_VALUE;
        int maxX = 0;
        int maxY = 0;
        for (Point p : area) {
            if (minX > p.x) {
                minX = p.x;
            }
            if (minY > p.y) {
                minY = p.y;
            }
            if (maxX < p.x) {
                maxX = p.x;
            }
            if (maxY < p.y) {
                maxY = p.y;
            }
        }
        return new Rectangle(minX, minY, maxX - minX, maxY - minY);
    }

    private List<Set<Point>> splitPixelsToAreas(List<Point> diffPoints) {
        List<Set<Point>> result = new ArrayList<>();
        List<Point> processed = new ArrayList<>();
        for (int i = 0; i < diffPoints.size(); i++) {
            Point current = diffPoints.get(i);
            if (!processed.contains(current)) {
                Set<Point> sq = collectPointsNearCurrent(diffPoints, processed, i, current);
                result.add(sq);
            }
        }
        return result;
    }

    private Set<Point> collectPointsNearCurrent(List<Point> diffPoints, List<Point> processed, int i, Point current) {
        Set<Point> area = new HashSet<>();
        for (int j = i + 1; j < diffPoints.size(); j++) {
            Point point = diffPoints.get(j);
            if (!processed.contains(point)) {
                if (isInSameArea(current, point)) {
                    area.add(point);
                    processed.add(point);
                    area.addAll(collectPointsNearCurrent(diffPoints, processed, j, point));
                }
            }
        }
        return area;
    }

    private boolean isInSameArea(Point a, Point b) {
        return a.distance(b) <= PIXEL_DISTANCE_CORRELATION;
    }
}

