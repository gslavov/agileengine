package org.slavov;

public class Main {

    public static void main(String[] args) {
        ImageCompare imageCompare = new ImageCompare();
        if (args.length == 2) {
            imageCompare.compare(args[0], args[1]);
        } else if (args.length == 3) {
            imageCompare.compare(args[0], args[1], args[2]);
        } else {
            System.out.println("Please specify both files");
        }
    }
}