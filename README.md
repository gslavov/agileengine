Result of usecase: job offer (Feb 2016)  

**Image Compare**
===============================================================================================
Program took three arguments:
1. Path to file with possible changes (required)
2. Path to original file (required)
3. Path to file that contains excluded parts (not required)

Also possible to exclude certain parts of the image from comparison. Create file and copy
[x=105, y=190, width=50, height=100] 
this is a pattern how to exclude parts from image comparison
and after run you can see excluded area marked with blue color.

------------------------------------------------------------------------------------------------
To run the program in command line go to folder where jar file is and run command:
java -jar ImageComparison.jar "C:\ImageWithChanges.png" "C:\OriginalImage.png"
and if you want to exclude certain part add 3rd argument:
java -jar ImageComparison.jar "C:\ImageWithChanges.png" "C:\OriginalImage.png" "C:\exclude.dat"

-------------------------------------------------------------------------------------------------
Output for this program will be "output.png" with different regions outlined with red rectangle